var expect = require('chai').expect;

// 模拟window对象
global.window = {};
var WSC = require('../src/web-smart-client');
global.window.WSC = WSC;
require('../src/web-smart-client-utils');

describe('WSC.util', function () {
    describe('#trim()', function () {
        it('当左侧有空格的字符串时，过滤掉左侧空格', function () {
            var trimStr = WSC.utils.trim(' test');

            expect(trimStr).to.equal('test');
        });

        it('当右侧有空格的字符串时，过滤掉右侧空格', function () {
            var trimStr = WSC.utils.trim('test ');

            expect(trimStr).to.equal('test');
        });

        it('当两侧有空格的字符串时，过滤掉两侧空格', function () {
            var trimStr = WSC.utils.trim(' test ');

            expect(trimStr).to.equal('test');
        });

        it('当中间有空格的字符串时，不过滤', function () {
            var trimStr = WSC.utils.trim(' te st ');

            expect(trimStr).to.equal('te st');
        });
    });

    describe('#type()', function () {
        it('当传入数组时，返回array字符串', function () {
            var ret = WSC.utils.type([]);

            expect(ret).to.equal('array');
        });

        it('当传入函数时，返回function字符串', function () {
            var ret = WSC.utils.type(function(){});

            expect(ret).to.equal('function');
        });

        it('当传入数字时，返回number字符串', function () {
            var ret = WSC.utils.type(2);

            expect(ret).to.equal('number');
        });

        it('当传入字符串时，返回string字符串', function () {
            var ret = WSC.utils.type('test');

            expect(ret).to.equal('string');
        });

        it('当传入true时，返回boolean字符串', function () {
            var ret = WSC.utils.type(true);

            expect(ret).to.equal('boolean');
        });
    });

    describe('#isArray()', function () {
        it('当传入数组时，返回true', function () {
            var ret = WSC.utils.isArray([]);

            expect(ret).to.true;
        });

        it('当传入字符串时，返回false', function () {
            var ret = WSC.utils.isArray('sss');

            expect(ret).to.false;
        });
    });

    describe('#objToParam()', function () {
        it('当传入简单对象时，返回url', function () {
            var ret = WSC.utils.obj2param({
                a: 'a1',
                b: 'b1'
            });

            expect(ret).to.equal('a=a1&b=b1');
        });

        it('当传入对象包含对象时，返回正确url', function () {
            var ret = WSC.utils.obj2param({
                a: {
                    c: 'c1',
                    d: 'd1'
                },
                b: 'b1'
            });

            expect(ret).to.equal('a%5Bc%5D=c1&a%5Bd%5D=d1&b=b1');
        });

        it('当传入对象包含数组时，返回正确url', function () {
            var ret = WSC.utils.obj2param({
                a: ['a1', 'a2'],
                b: 'b1'
            });

            expect(ret).to.equal('a%5B%5D=a1&a%5B%5D=a2&b=b1');
        });
    });

    describe('#cloneObj()', function () {
        it('当克隆对象时，产生一个新对象，且属性相同', function () {
            var obj = {
                name: 'demo'
            };
            var newObj = WSC.utils.cloneObj(obj);

            expect(newObj === obj).to.false;

            expect(newObj).to.have.property('name').with.equal('demo');
        });
    });

    describe('#extendObj()', function () {
        it('当扩展对象时，目标对象会产生新的属性', function () {
            var obj = {
                name: 'demo'
            };

            var destObj = {
                'age': 20
            }

            var newObj = WSC.utils.extendObj(destObj, obj);

            expect(newObj === destObj).to.true;
            expect(destObj).to.have.property('name').with.equal('demo');
            expect(destObj).to.have.property('age').with.equal(20);
        });
    });

    describe('#parseJSON()', function () {
        it('当传入json时，会解析未json对象', function () {
            var obj = WSC.utils.parseJSON('{"name": "demo"}');

            expect(obj).to.be.an('object');
            expect(obj).to.have.property('name').with.equal('demo');
        });
    });

    describe('#wrapApi()', function () {
        it('当包装另一个函数时，成功包装', function () {
            var wrapFn = WSC.utils.wrapApi(function (msg) {
                return 'my name is ' + msg;
            }, function (done) {
                var ret = done();

                return 'hi, ' + ret;
            });

            var ret = wrapFn('demo');

            expect(ret).to.equal('hi, my name is demo');
        });

        it('当包装另一个函数、并传入上下文时，使用对应的上下文信息', function () {
            var a = {
                hi: 'hi',
                say: function (name) {
                    return this.hi + ' ' + name;
                }
            }

            var b = {
                hi: 'hello',
                wrap: function (done) {
                    return '[' + this.hi + '] ' + done();
                }
            }

            var wrapFn = WSC.utils.wrapApi(a.say, b.wrap, a, b);

            var ret = wrapFn('demo');

            expect(ret).to.equal('[hello] hi demo');
        });
    });

    describe('#isError()', function () {
        it('当传入Error时，返回true', function () {
            var ret = WSC.utils.isError(Error('error-msg'));

            expect(ret).to.be.true;
        });

        it('当传入Function时，返回false', function () {
            var ret = WSC.utils.isError(function(){});

            expect(ret).to.be.false;
        });

        it('当传入数字时，返回false', function () {
            var ret = WSC.utils.isError(123);

            expect(ret).to.be.false;
        });

        it('当传入字符串时，返回false', function () {
            var ret = WSC.utils.isError('abc123');

            expect(ret).to.be.false;
        });

        it('当传入对象时，返回false', function () {
            var ret = WSC.utils.isError({});

            expect(ret).to.be.false;
        });
    });

    describe('#isFunction()', function () {
        it('当传入Function时，返回true', function () {
            var ret = WSC.utils.isFunction(function(){});

            expect(ret).to.be.true;
        });

        it('当传入Error时，返回false', function () {
            var ret = WSC.utils.isFunction(Error('error-msg'));

            expect(ret).to.be.false;
        });

        it('当传入数字时，返回false', function () {
            var ret = WSC.utils.isFunction(123);

            expect(ret).to.be.false;
        });

        it('当传入字符串时，返回false', function () {
            var ret = WSC.utils.isFunction('abc123');

            expect(ret).to.be.false;
        });
    });
});