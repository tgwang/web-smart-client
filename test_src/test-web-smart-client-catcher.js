var expect = require('chai').expect;

// 模拟window对象
global.window = {};
var WSC = require('../src/web-smart-client');
global.window.WSC = WSC;
require('../src/web-smart-client-utils');
require('../src/web-smart-client-logger');
require('../src/web-smart-client-catcher');

describe('WSC.catcher', function () {
    before(function () {
        WSC.config({
            'logger-url': 'http://127.0.0.1/log'
        });
    });
    describe('#defaultCatchHandler()', function () {

        it('当使用默认异常处理函数时，记录日志成功', function () {
            function test() {
                var e = d / f;
            }

            test = WSC.catcher.tryFunction(test);
            try {
                test();
            } catch (e) {
                expect(e).to.not.null;
            }
        });

        it('当设置默认异常处理函数时，设置成功', function () {
            var fn = function (e) {
                return 'e';
            };

            var defaultCatchHandler = WSC.catcher.defaultCatchHandler(fn);

            expect(defaultCatchHandler._catchHandler_).to.equal(fn);
        });
    });

    describe('#catchHandler()', function () {
        it('当设置异常处理函数时，设置成功', function () {
            var fn = function (done) {
                done();
            };

            var catchHandler = WSC.catcher.catchHandler(fn);

            expect(catchHandler._catchHandler_).to.equal(fn);
        });

        it('当设置异常处理函数且发生异常时，正常进入处理函数中', function () {
            var fn = function (e) {
                console.log(String(e));
                WSC.logger.error(e);
                expect(e).to.not.null;
            };

            var catchHandler = WSC.catcher.catchHandler(fn);

            function test() {
                var c = a / b;
            }
            test = catchHandler.tryFunction(test);
            try {
                test()
            } catch (e) {
                console.log('---', e);
            }
        });
    });

    describe('#tryFunction()', function () {
        it('当函数异常时，返回错误', function (done) {
            var fn = function (e) {
                expect(e).to.equal('test_30');
                done();
            };

            var tarFn = function (name, age) {
                if(name == 'demo') {
                    return name + ':' + age;
                } else {
                    throw name + '_' + age;
                }
            }

            var catchHandler = WSC.catcher.catchHandler(fn);

            tarFn = catchHandler.tryFunction(tarFn);

            tarFn('test', '30')
        });

        it('当函数异常（带回调函数）时，返回错误', function (done) {
            var fn = function (e) {

                WSC.logger.error('异常', e);
                expect(e).to.equal('error test:22');
                done();
            };

            var tarFn = function (name, age, cb) {
                cb(name, name + ':' + age);
            }

            var catchHandler = WSC.catcher.catchHandler(fn);

            tarFn = catchHandler.tryFunction(tarFn);

            var cb = function(name, txt){
                throw 'error ' + txt;
            }

            tarFn('test', '22', cb);
        });
    });
});