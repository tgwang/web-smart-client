var expect = require('chai').expect;

// 模拟window对象
global.window = {};
var WSC = require('../src/web-smart-client');

describe('WSC', function () {

    describe('#config()', function () {

        it('当覆盖配置时，应该更新未新的配置信息', function () {
            WSC.config({
                id: 'test'
            });

            expect(WSC._config_).to.have.property('id').with.equal('test');
        });

        it('当增加配置时，会增加新的配置项', function () {
            WSC.config({
                name: 'config'
            });

            expect(WSC._config_).to.have.property('name').with.equal('config');
        });

        it('当再次增加配置时，原来的配置不能改变', function () {
            WSC.config({
                desc: 'this is config'
            });

            expect(WSC._config_).to.have.property('name').with.equal('config');
        });
    });

    describe('#namespace()', function () {
        it('当新建namespace时，返回新的命名空间', function () {
            var logger = WSC.namespace('logger');

            expect(logger).to.not.be.undefined;
            expect(logger._name_).to.equal('logger');
        });
    });

    describe('#ns()', function () {
        it('当新建namespace时，返回新的命名空间', function () {
            var logger = WSC.ns('test');

            expect(logger).to.not.be.undefined;
            expect(logger._name_).to.equal('test');
        });
    });
});

describe('Namespace', function () {

    describe('#extend()', function () {
        it('当新建单个方法时，应该成功添加', function () {
            var logger = WSC.namespace('logger').extend('info', function (msg) {
                return msg;
            });

            var ret = WSC.logger.info('test');

            expect(ret).to.equal('test');
        });

        it('当覆盖已存在的方法时，应该成功覆盖', function () {
            var logger = WSC.namespace('logger').extend('info', function (msg) {
                return msg + '-info';
            });

            var ret = WSC.logger.info('test');

            expect(ret).to.equal('test-info');
        });

        it('当新建方法集合时，应该成功添加', function () {
            var logger = WSC.namespace('logger').extend({
                debug: function (msg) {
                    return msg + '-debug'
                },
                error: function (msg) {
                    return msg + '-error'
                }
            });

            var ret = WSC.logger.debug('test');
            expect(ret).to.equal('test-debug');

            ret = WSC.logger.error('test');
            expect(ret).to.equal('test-error');
        });
    });

    describe('#config()', function () {
        it('当新增配置时，应该成功添加', function () {
            var logger = WSC.namespace('logger').config({
                'url': 'http'
            });

            var ret = WSC.logger._config_['url'];

            expect(ret).to.equal('http');
        });

        it('当覆盖配置时，应该成功覆盖', function () {
            var logger = WSC.namespace('logger').config({
                'url': 'http-1'
            });

            var ret = WSC.logger._config_['url'];

            expect(ret).to.equal('http-1');
        });
    });

    describe('#getConfig()', function () {
        it('当获取配置时，应该成功获取', function () {
            var logger = WSC.namespace('logger').config({
                'url': 'http'
            });

            var ret = WSC.logger.getConfig('url');

            expect(ret).to.equal('http');
        });

        it('当ns对象配置没有时，应该到父对象wsc中查找', function () {
            var ret = WSC.logger.getConfig('id');

            expect(ret).to.equal('test');
        });

        it('当获取配置ns对象没有时，应该到父对象wsc中查找', function () {
            var ret = WSC.logger.getConfig('id');

            expect(ret).to.equal('test');
        });
    });
    
});