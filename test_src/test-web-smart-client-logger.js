var expect = require('chai').expect;

// 模拟window对象
global.window = {};
var WSC = require('../src/web-smart-client');
global.window.WSC = WSC;
require('../src/web-smart-client-utils');
require('../src/web-smart-client-logger');

describe('WSC.logger', function () {
    before(function(){
        WSC.config({
            'logger-url': 'http://127.0.0.1/test'
        });
    });
    describe('#debug()', function () {
        it('当记录debug日志，正常返回', function(done){
            WSC.logger.debug('debug-info', function(e){
                expect(e.type).to.equal('error');
                done()
            });
        });
    });

    describe('#info()', function () {
        it('当记录info日志，正常返回', function(done){
            WSC.logger.info('info-info', function(e){
                expect(e.type).to.equal('error');
                done()
            });
        });
    });

    describe('#warn()', function () {
        it('当记录warn日志，正常返回', function(done){
            WSC.logger.warn('warn-info', function(e){
                expect(e.type).to.equal('error');
                done()
            });
        });
    });

    describe('#error()', function () {
        it('当传入两个参数且msg传字符串时，消息正常返回', function(done){
            WSC.logger.error('error-info', function(e){
                expect(e.type).to.equal('error');
                done()
            });
        });

        it('当传入两个参数且msg传Error时，消息正常返回', function(done){
            WSC.logger.error(Error('error-msg'), function(e){
                expect(e.type).to.equal('error');
                done()
            });
        });

        it('当传入两个参数且msg传对象时，消息正常返回', function(done){
            WSC.logger.error({
                msg: 'error-obj-msg',
                ext: 'ext-info'
            }, function(e){
                expect(e.type).to.equal('error');
                done()
            });
        });

        it('当传入三个参数且msg传字符串时，消息正常返回', function(done){
            WSC.logger.error('error-info', Error('error-msg'),function(e){
                expect(e.type).to.equal('error');
                done()
            });
        });

        it('当传入三个参数且msg传对象时，消息正常返回', function(done){
            WSC.logger.error({
                msg: 'error-obj-msg',
                ext: 'ext-info'
            }, Error('error-msg'),function(e){
                expect(e.type).to.equal('error');
                done()
            });
        });

        
        it('当传入两个参数且msg传字符串时(不带回调)，消息正常返回', function(){
            WSC.logger.error('error-info');
            expect(true).to.be.true;
        });

        it('当传入两个参数且msg传Error时(不带回调)，消息正常返回', function(){
            WSC.logger.error(Error('error-info'));
            expect(true).to.be.true;
        });

        it('当传入两个参数且msg传对象时(不带回调)，消息正常返回', function(){
            WSC.logger.error({
                msg: 'no callback fn'
            });
            expect(true).to.be.true;
        });
    });
});