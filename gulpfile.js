'use strict'

// const path = require('path');
const gulp = require('gulp');
const clean = require('gulp-clean');
const source = require('vinyl-source-stream');
const rename = require('gulp-rename');
const gulpSequence = require('gulp-sequence');

const browserify = require('browserify');
const glob = require('glob');
const es = require('event-stream');

gulp.task('clean-static-dist', function () {
    return gulp.src('test_web')
        .pipe(clean());
});

gulp.task('build-js', function (done) {
    glob('./test_src/test-*.js', {}, function (err, files) {
        if (err) done(err);

        let tasks = files.map(function (entry) {
            return browserify({ entries: [entry] })
                .bundle()
                .pipe(source(entry))
                .pipe(rename(function (path) {
                    path.dirname = path.dirname.replace('test_src', '')
                }))
                .pipe(gulp.dest('test_web'));
        });

        es.merge(tasks).on('end', done);
    });

});

gulp.task('build-html', function () {
    gulp.src('./test_src/test.html')
        .pipe(gulp.dest('test_web'));
});

gulp.task('default', ['clean-static-dist'], gulpSequence(['build-js', 'build-html']));