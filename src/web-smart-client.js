; (function (window) {

    var core_verson = '0.9.0';

    // 断言
    function assert(value, msg) {
        if (!value) {
            throw msg;
        }
    }

    function isObject(value) {
        return Object.prototype.toString.call(value) === '[object Object]';
    }

    function isFunction(value) {
        return Object.prototype.toString.call(value) === '[object Function]';
    }

    // 构造WebSmartClient类
    function WebSmartClient() {
        this._config_ = {
            id: '',
            url: ''
        };
    }

    function Namespace(name, _super) {
        this._name_ = name;
        this._super_ = _super;
        this._config_ = {};
    }

    // 扩展命名空间
    Namespace.prototype = {
        // 版本
        namespace: core_verson,

        // 构造函数
        constructor: Namespace,

        /**
         * @param name 方法名
         * @param fn 方法体
         */
        extend: function (name, fn) {
            assert(arguments.length !== 0, "参数不能为空");

            var fns = {};

            // 如果一个参数，表示方法集合对象
            if (arguments.length == 1) {
                assert(isObject(arguments[0]), "参数格式不正确")
                fns = name;
            } else {
                fns[name] = fn;
            }

            for (var fnName in fns) {
                this[fnName] = fns[fnName];
            }

            return this;
        },

        // 配置
        config: function (cfgs) {
            if (isFunction(cfgs)) {
                cfgs = cfgs.call(this);
            }

            for (var cfgName in cfgs) {
                this._config_[cfgName] = cfgs[cfgName];
            }

            return this;
        },

        // 获取配置
        getConfig: function (cfgName) {
            if (cfgName in this._config_) {
                return this._config_[cfgName];
            }

            // 如果没有，到父对象中找
            if (cfgName in this._super_._config_) {
                return this._super_._config_[cfgName];
            }

            return;
        }
    };

    WebSmartClient.prototype = {
        // 版本
        wsc: core_verson,

        // 构造函数
        constructor: WebSmartClient,

        // 配置
        config: function (cfgs) {
            for (var cfgName in cfgs) {
                this._config_[cfgName] = cfgs[cfgName];
            }

            return this;
        },

        // 命名空间
        namespace: function (name) {
            assert(name, "namespace name 不能为空");

            // 如果已经存在，直接返回
            if (this[name]) {
                if (!(this[name] instanceof Namespace)) {
                    throw '此名称不能当作namespace';
                }

                return this[name];
            }

            // 如果不存在，创建ns
            this[name] = new Namespace(name, this);

            return this[name];
        },

        // 命名空间简写
        ns: function (name) {
            return this.namespace(name);
        }
    };

    var WSC = new WebSmartClient();

    if (typeof module === "object" && module && typeof module.exports === "object") {
        module.exports = WSC;
    } else {
        window.WSC = WSC;
    }
})(window);