; (function (window) {
    if (!window.WSC) {
        throw '请先引入WSC文件';
    }

    var wsc = window.WSC;
    var plugin = 'utils';

    // 匹配前后空格
    var whitespace = "[\\x20\\t\\r\\n\\f]",
        rtrim = new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g");
    var core_trim = plugin.trim;

    // json字符串
    var rvalidchars = /^[\],:{}\s]*$/,
        rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g,
        rvalidescape = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
        rvalidtokens = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g;

    // trim函数
    var trim = core_trim && !core_trim.call("\uFEFF\xA0") ?
        function (text) {
            // 如果已经支持原生的 String 的 trim 方法
            return text == null ?
                "" :
                core_trim.call(text);
        } :
        function (text) {
            // 不支持原生的 String 的 trim 方法
            return text == null ?
                "" :
                // text + "" 强制类型转换 ，转换为 String 类型
                (text + "").replace(rtrim, "");
        };

    var class2type = {};

    var typeStrs = "Boolean Number String Function Array Date RegExp Object Error".split(" ");
    for (index in typeStrs) {
        class2type["[object " + typeStrs[index] + "]"] = typeStrs[index].toLowerCase();
    }

    function type(obj) {
        if (obj == null) {
            return String(obj);
        }

        return class2type[Object.prototype.toString.call(obj)];
    }

    // is函数集合，注意闭包作用域
    var isFunctionMap = {};
    for (index in typeStrs) {
        isFunctionMap['is' + typeStrs[index]] = (function (i) {
            return function(obj) {
                return type(obj) === typeStrs[i].toLowerCase();
            }
        })(index);
    }

    var isArray = Array.isArray || isFunctionMap['isArray'];

    /**
 * 构建参数
 * @param {*} prefix 
 * @param {*} obj 
 * @param {*} add 
 */
    function buildParams(prefix, obj, add) {
        let name;
        let rbracket = /\[\]$/;

        if (isArray(obj)) {

            // 遍历数组
            for (var index in obj) {
                var value = obj[index];

                if (rbracket.test(prefix)) {
                    // 直接序列化
                    add(prefix, value);
                } else {

                    // 需要遍历数组项，使用下标做前缀
                    buildParams(
                        prefix + "[" + (typeof value === "object" && value != null ? index : "") + "]",
                        value,
                        add
                    );
                }
            }

        } else if (typeof obj === "object") {

            // 遍历对象
            for (name in obj) {
                buildParams(prefix + "[" + name + "]", obj[name], add);
            }

        } else {

            // 普通字符串
            add(prefix, obj);
        }
    }

    function getXhr() {
        var xhr;
        if (window.XMLHttpRequest) { // Mozilla, Safari...
            xhr = new XMLHttpRequest();
        } else if (window.ActiveXObject) { // IE
            try {
                xhr = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                try {
                    xhr = new ActiveXObject('Microsoft.XMLHTTP');
                } catch (e) {
                    throw 'xhr failed';
                }
            }
        }

        throw "不支持ajax";
    }

    var extendMap = {
        trim: trim,
        type: type,
        obj2param: function (obj) {
            var prefix,
                params = [],
                add = function (key, value) {
                    params[params.length] = encodeURIComponent(key) + "=" +
                        encodeURIComponent(value == null ? "" : value);
                };

            for (prefix in obj) {
                buildParams(prefix, obj[prefix], add);
            }

            // Return the resulting serialization
            return params.join("&");
        },

        cloneObj: function (obj) {
            if (this.type(obj) != 'object' || obj == null) {
                return obj
            }

            var newObj = new Object();
            for (var i in obj) {
                newObj[i] = this.cloneObj(obj[i]);
            }

            return newObj;
        },

        /**
         * 扩展对象
         */
        extendObj: function (destObj, obj) {

            if (obj) {
                for (var k in obj) {
                    if (typeof obj[k] !== 'undefined') destObj[k] = obj[k];
                }
            }

            return destObj;
        },

        /**
         * 包装api函数
         * @param targetFn [必选] 目标函数
         * @param wrapFn [必选] 包装函数，只有一个参数：`done`，要执行目标函数时，执行`done()`[不需要参数]，可接受返回数据，并可以处理
         * @param context [非必选] 上下文环境，默认window环境
         * @param wrapContext [非必选] 包装器上下文环境，默认window环境
         */
        wrapApi: function (targetFn, wrapFn, context, wrapContext) {
            function doneWrapper() {
                var args = arguments;
                function done() {
                    return targetFn.apply(context, args);
                }
                return done;
            }

            return function () {
                return wrapFn.call(wrapContext || window, doneWrapper.apply(context  || window, arguments));
            }
        },

        /**
         * 解析XML字符串
         * @param data [必选] xml字符串
         */
        parseXML: function (data) {
            var xml, tmp;
            if (!data || typeof data !== "string") {
                return null;
            }
            try {
                if (window.DOMParser) { // Standard
                    tmp = new DOMParser();
                    xml = tmp.parseFromString(data, "text/xml");
                    console.log(xml);
                } else { // IE
                    xml = new ActiveXObject("Microsoft.XMLDOM");
                    xml.async = "false";
                    xml.loadXML(data);
                }
            } catch (e) {
                xml = undefined;
            }
            if (!xml || !xml.documentElement || xml.getElementsByTagName("parsererror").length) {
                throw "Invalid XML: " + data;
            }
            return xml;
        },

        /**
         * 解析 JSON 字符串
         * @param data JSON字符串
         */
        parseJSON: function (data) {
            if (window.JSON && window.JSON.parse) {
                return window.JSON.parse(data);
            }

            if (data === null) {
                return data;
            }

            if (typeof data === "string") {
                data = this.trim(data);

                if (data) {
                    // Make sure the incoming data is actual JSON
                    // Logic borrowed from http://json.org/json2.js
                    if (rvalidchars.test(data.replace(rvalidescape, "@")
                        .replace(rvalidtokens, "]")
                        .replace(rvalidbraces, ""))) {

                        return (new Function("return " + data))();
                    }
                }
            }

            throw "Invalid JSON: " + data;
        },

        obj2UrlParam: function (obj) {
            if (!obj) return '';

            var params = [];
            if (obj) {
                for (var k in obj) {
                    if (typeof obj[k] !== 'undefined') {
                        params.push(encodeURIComponent(k) + '=' + encodeURIComponent(obj[k]));
                    }
                }
            }

            params = params.join('&');

            return params;
        },

        // ajax
        ajax: function (options) {
            var xhr = getXhr();

            var dataType = (options.dataType || 'json').toLowerCase();

            xhr.onreadystatechange = function () {
                // 该函数会被调用四次
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        var resp = xhr.responseText;
                        try {
                            if (dataType === 'json') {
                                resp = this.parseJSON(xhr.responseText);
                            } else if (dataType === 'xml') {
                                resp = this.parseXML(xhr.responseText);
                            }
                        } catch (e) {
                        }
                        options.success && options.success(resp, xhr);
                    } else {
                        options.failure && options.failure(xhr);
                    }
                }
            }

            var method = (options.method || 'GET').toUpperCase();

            var params = [];

            if (options.params) {
                for (var k in options.params) {
                    if (typeof options.params[k] !== 'undefined') {
                        params.push(encodeURIComponent(k) + '=' + encodeURIComponent(options.params[k]));
                    }
                }
            }

            params = params.join('&');

            var url = options.url || '';

            var seq = url.indexOf('?') >= 0 ? '&' : '?';

            var body = null;
            if (method === 'GET') {
                url = url + seq + params;
            } else {
                body = params;
            }

            xhr.open(method, url, true);

            var headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

            this.extendObj(headers, options.headers);

            // 设置headers
            for (var headerName in headers) {
                xhr.setRequestHeader(headerName, headers[headerName]);
            }

            xhr.send(body);
        }
    };

    // 添加is函数
    for (key in isFunctionMap) {
        extendMap[key] = isFunctionMap[key];
    }

    // isArray能使用原生的函数
    extendMap['isArray'] = isArray;

    wsc.ns(plugin).extend(extendMap);
})(window);