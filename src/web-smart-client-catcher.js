; (function (window) {
    if (!window.WSC) {
        throw '请先引入WSC文件';
    }

    var wsc = window.WSC;
    var plugin = 'catcher';

    function CatcherHandler(handler) {
        this._catchHandler_ = handler;
    }

    CatcherHandler.prototype = {
        constructor: CatcherHandler,

        tryFunction: function (fn, context, tryArgs) {
            function tryWrapper(done) {
                try {
                    return done();
                } catch (e) {
                    this._catchHandler_ && this._catchHandler_(e);
                }
            }

            // 如果传入tryArgs，将参数中的函数也使用try函数进行包裹

            var self = this;
            return function () {

                var arg, args = [];
                if (tryArgs) {
                    for (var i = 0, len = arguments.length; i < len; i++) {
                        arg = arguments[i];
                        wsc.utils.isFunction(arg) && (arg = wsc.utils.wrapApi(arg));
                        args.push(arg);
                    }
                } else {
                    args = arguments;
                }

                return wsc.utils.wrapApi(fn, tryWrapper, context, self).apply(wsc.utils, args);

            }
        }
    }

    function defaultTryCatcherHandler(e) {
        wsc.logger.error(e);
        throw e;
    }

    var defaultCatcherHandler = new CatcherHandler(defaultTryCatcherHandler);

    wsc.ns(plugin).extend({
        defaultCatchHandler: function (handler) {
            defaultCatcherHandler._catchHandler_ = handler;
            return defaultCatcherHandler;
        },

        catchHandler: function (handler) {
            return new CatcherHandler(handler);
        },

        tryFunction: function (fn, context) {
            return defaultCatcherHandler.tryFunction(fn, context);
        },

    });
})(window);