; (function (window) {
    if (!window.WSC) {
        throw '请先引入WSC文件';
    }

    var wsc = window.WSC;
    var plugin = 'logger';

    function createImg(src) {
        var img = window.document.createElement('img');
        img.width = 1;
        img.height = 1;
        img.src = src;
        return img;
    };

    function imgSend(url, param, callback) {
        var img = createImg(url + '?' + param);
        img.onload = img.onerror = function (r) {
            img.onload = null;
            img.onerror = null;
            callback && callback(r);
        }
    }

    var INFO_LEVEL = {
        debug: '1',
        info: '2',
        warn: '3',
        error: '4'
    }

    var infoMsg = {
        m: 'log',           // 方法
        l: undefined,       // 消息等级
        f: undefined,       // from地址
        t: undefined,       // 时间
        msg: undefined,     // 消息
        row: undefined,     // 行
        col: undefined,     // 列
        ext: undefined      // 额外参数
    };

    function getMsg(level, msg) {
        var msgInfo = {
            l: level,
            f: window.location.href,
            t: new Date().getTime()
        };

        wsc.utils.extendObj(msgInfo, infoMsg);

        if (wsc.utils.isString(msg)) {
            msgInfo['msg'] = msg;
        } else if (wsc.utils.isObject(msg)) {
            msgInfo['msg'] = msg['msg'];
            msgInfo['row'] = msg['row'];
            msgInfo['col'] = msg['col'];
            msgInfo['ext'] = msg['ext'];
        } else {
            msgInfo['msg'] = String(msg);
        }

        return msgInfo;
    }

    function sendMsg(url, level, msg, callback) {
        var msgInfo = getMsg(level, msg);

        var param = wsc.utils.obj2param(msgInfo);

        imgSend(url, param, callback);
    }

    // 参考badjs处理异常信息：https://github.com/BetterJS/badjs-report
    function processError(errObj) {
        try {
            if (errObj.stack) {
                var url = errObj.stack.match("[https?|file]://[^\n]+");
                url = url ? url[0] : "";
                var rowCols = url.match(":(\\d+):(\\d+)");
                if (!rowCols) {
                    rowCols = [0, 0, 0];
                }

                var stack = processStackMsg(errObj);
                return {
                    msg: stack,
                    row: rowCols[1],
                    col: rowCols[2]
                };
            } else {
                //ie 独有 error 对象信息，try-catch 捕获到错误信息传过来，造成没有msg
                if (errObj.name && errObj.message && errObj.description) {
                    return {
                        msg: JSON.stringify(errObj)
                    };
                }

                return {
                    msg: errObj
                }
            }
        } catch (err) {
            return {
                msg: errObj
            }
        }
    }

    function processStackMsg(error) {
        var stack = error.stack
            .replace(/\n/gi, "")
            .split(/\bat\b/)
            .slice(0, 9)
            .join("@")
            .replace(/\?[^:]+/gi, "");
        var msg = error.toString();
        if (stack.indexOf(msg) < 0) {
            stack = msg + "@" + stack;
        }
        return stack;
    }

    var loggerUrl = plugin + '-url'

    wsc.ns(plugin).extend({
        debug: function (msg, callback) {
            var url = wsc.logger.getConfig(loggerUrl) || '';

            sendMsg(url, INFO_LEVEL.debug, msg, callback);
        },
        info: function (msg, callback) {
            var url = wsc.logger.getConfig(loggerUrl) || '';

            sendMsg(url, INFO_LEVEL.info, msg, callback);
        },
        warn: function (msg, callback) {
            var url = wsc.logger.getConfig(loggerUrl) || '';

            sendMsg(url, INFO_LEVEL.warn, msg, callback);
        },
        error: function (msg, err, callback) {
            var url = wsc.logger.getConfig(loggerUrl) || '';

            // 第一个参数是Error类型，只能有两个参数
            if (msg instanceof Error) {

                if (err && !wsc.utils.isFunction(err)) {
                    throw "参数格式不正确";
                }

                callback = err;
                err = msg;
                msg = undefined;
            } else {
                // 如果第二个参数是
                if (err) {
                    if (wsc.utils.isFunction(err)) {
                        callback = err;
                        err = undefined;
                    }
                }
            }

            var errInfo = {};
            if (err) {
                errInfo = processError(err);
            }

            if (msg) {
                var _msg = '';
                if (wsc.utils.isObject(msg)) {
                    _msg = msg['msg'];
                    delete msg['msg'];

                    wsc.utils.extendObj(errInfo, msg);
                } else {
                    _msg = msg;
                }

                errInfo['msg'] = String(_msg) + (errInfo['msg'] ? ' || ' + errInfo['msg'] : '')
            }

            sendMsg(url, INFO_LEVEL.error, errInfo, callback);
        }
    });
})(window);