# web分析器客户端

## 说明

web分析器主要用于为前端用户环境监控提供解决方案，方便服务人员对前端环境进行监控，了解前端功能在各个用户处执行情况。

web分析器主要功能有：

1. 用户环境错误监控
1. 用户环境日志报告
1. 用户环境性能分析
1. 提供插件环境，可自定义插件

web分析器的目标是提供简单的api接口，方便进行web开发人员进行前端日志记录和错误监控。

web分析器提供api函数包装方法，便于web开发人员对指定函数进行错误监控和性能监控，利用AOP（面向切面编程）思想进行相关信息记录。

## api

### 配置

用于配置web分析器的相关参数，id为每个统计的标识，必须配置，如果不配置则不向后台发送统计数据

示例：

```js
WSC.config({
    id: '',
});
```

### 日志记录

用于向后台记录日志，方便后台统计相关信息

示例：

```js
WSC.logger.debug('')
...
...
WSC.logger.error('', e);
```

#### debug

记录debug级别日志

1. 参数：
    - `msg`: _[必选]_ 调试消息

1. 示例：

    ```js
    WSC.logger.debug('this is debug')
    ```

#### info

记录info级别日志

1. 参数：
    - `msg`: _[必选]_ 普通消息

1. 示例：

```js
WSC.logger.info('this is info')
```

#### warn

记录warn级别日志

1. 参数：
    - `msg`: _[必选]_ 警告消息

1. 示例：

```js
WSC.logger.warn('this is warn')
```

#### error

记录error级别日志

1. 参数：
    - `msg`: _[必选]_ 错误消息
    - `e`: _[非必选]_ error异常堆栈信息

1. 示例：

```js
WSC.logger.error('this is error')

try {
    ...
} catch(Error e) {
    WSC.logger.error('this is error', e)
}
```

### 工具类

简化操作的工具类

示例：

```js
WSC.utils.wrapApi(targetFn, wrapFn, context, wrapContext);
```

#### trim

过滤字符串

1. 参数：
    - `text`: 字符串

1. 示例：

```js
var trimStr = WSC.utils.trim(' text ')
```

#### cloneObj

克隆对象

1. 参数：
    - `obj`: 对象

1. 示例：

```js
var newObj = WSC.utils.cloneObj({key: 'value'})
```

#### extendObj

扩展对象

1. 参数：
    - `destObj`: 目标对象
    - `obj`: 对象

1. 示例：

```js
var obj = {key: 'value'}

WSC.utils.extendObj(obj, {
    name: 'test'
})
```

#### wrapApi

包装api函数的工具函数，使用wrapFn包装目标函数

1. 参数：
    - `targetFn`: _[必选]_ 目标函数
    - `wrapFn`: _[必选]_ 包装函数，只有一个参数：`done`，要执行目标函数时，执行`done()`[不需要参数]，可接受返回数据，并可以处理
    - `context`: _[非必选]_ 上下文环境，默认window环境
    - `wrapContext`: _[非必选]_ 包装器上下文环境，默认window环境

1. 示例：
    1. 全局函数

        ```js
        WSC.utils.wrapApi(function(msg){
            console.log(msg);
        }, function(done){
            try {
                done();
            } catch(e) {
                console.error(e);
            }
        });
        ```

    1. 有上下文的函数
        ```js
        var obj = {
            data: 'obj',
            alert: function(txt) {
                console.log(this.data + ' -> ' + txt);
            },
            wrapper: function(done) {
                console.log('wrap...' + this.data);
                done();
            }
        }

        WSC.utils.wrapApi(obj.alert, obj.wrapper, obj, obj)
        ```

#### parseXML

将xml字符串转换成xml结构

1. 参数：
    - `data`: xml字符串

1. 示例：

```js
WSC.utils.parseXML('<xml><name>test</name><age>19</age></xml>')
```

#### parseJSON

将json字符串转换成json对象

1. 参数：
    - `data`: json字符串

1. 示例：

```js
WSC.utils.parseJSON('{key: "value"}')
```

#### ajax

发送ajax请求

1. 参数：
    - `options`: 参数对象
        - `method`: http方法，`GET`、`POST`
        - `url`: http地址
        - `headers`: http头，_[对象]_
        - `params`: http参数，_[对象]_
        - `success`: http成功函数
        - `failure`: http失败函数

1. 示例：

```js
WSC.utils.ajax({
    url: '/api',
    method: 'GET',
    headers: {
        'Auth': 'test'
    },
    params: {
        name: 'demo'
    },
    success: function(resp, xhr){
        console.log(resp, xhr);
    },
    failure: function(xhr){
        console.log(xhr);
    }
})
```

### 异常捕获

对常见的模块进行异常捕获

示例：

```js
WSC.catcher.catchHandler(function(e) {
...
}).wrapTryModules().wrapTrySystem().tryFunction(function(){
...
});
```

### defaultCatchHandler

默认的异常处理函数，如有多个defaultCatchHandler，则会覆盖，已最后一个定义的为准，也可使用catchHandler对某一个try处理进行定制化处理

1. 参数：
    - `handler`: _[必选]_ 异常处理函数，参数`e`表示具体错误信息

1. 示例：

```js
WSC.catcher.defaultCatchHandler(function(e){
    ...
})
```

#### catchHandler

对异常捕获的进行处理，如果定义将覆盖defaultCatchHandler的函数，此类定义只能进行作用在其后续的管道中，脱离此管道将不会使用此catchHandler

如：

```js
var handler = WSC.catcher.catchHandler(function(e){
    ...
})

// 此类try的函数时会用用catchHandler中定义的异常处理器进行处理
handler.tryFunction(function(){
    ...
});

// 此类try的函数时会用defaultCatchHandler中定义的异常处理器进行处理
WSC.catcher.tryFunction(function(){
    ...
})
```

1. 参数：
    - `handler`: _[必选]_ 异常处理函数，参数`e`表示具体错误信息

1. 示例：

```js
WSC.catcher.catchHandler(function(e){
    ...
})
```

#### tryFunction

使用try包裹目标函数，可以使用catchHandler进行自定义处理

1. 参数：
    - `fn`: _[必选]_ 需要包裹的函数

1. 示例：
    ```js
    WSC.catcher.tryFunction(function(){
        ...
    })
    ```

#### wrapTryModules

包装前端模块的定义和引用方法，针对`define`和`require`进行封装

1. 参数：(无)

1. 示例：
    ```js
    WSC.catcher.wrapTryModules()
    ```

### 扩展

用于扩展web分析器的功能

示例：

```js
WSC.namespace('logger').extend('debug', function(msg) {
    ...
});
```

#### namespace <-> ns

新建命名空间或获取已存在的命名空间，用于对此命名空间进行扩展方法，namespace和ns作用时一样的，ns时namespace的简写

1. 参数：
    - `name`: _[必选]_ 命名空间名称

1. 示例：

    ```js
    WSC.namespace('logger')
    WSC.ns('logger')
    ```

#### extend

扩展命名空间的方法

1. 参数：
    1. 添加单个方法
        - `name`: 方法名
        - `fn`: 方法体
    1. 添加方法集合对象
        - `fns`: 方法集合对象

1. 示例：
    1. 添加单个方法

        ```js
        WSC.ns('logger').extend('debug', function(msg){
            ...
        })
        ```
    1. 添加方法集合对象
        ```js
        WSC.ns('logger').extend({
            debug: function(msg) {
                ...
            },
            info: function(msg){
                ...
            }
        })
        ```